var bddPluginApp = angular.module('bddPluginApp', []);

bddPluginApp.directive('ready', ['$rootScope', function($rootScope) {
    return {
        restrict: 'A',
        link: function($scope) {
            preparePlugin();
        }
    };
}]);

var useCaseTabScope;
var configTabScope;
var testResultsTabScope;
var resultsSummaryScope;
var logsSummaryScope;

var mantisDir;
var mantisRootUrl;
var currentUser;
var projectNameSpace;