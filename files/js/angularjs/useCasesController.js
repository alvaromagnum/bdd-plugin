bddPluginApp.controller('UseCasesController', function ($scope) {

    $scope.useCases = [];
    $scope.useCaseLangage = "CSharp";
    $scope.editMode = false;
    $scope.idUseCaseToEdit = 1;

    $scope.getCardClassByPriority = function(priority){
        return getCardClassByPriority(priority);
    };

    $scope.setUseCaseLanguage = function(language) {
        setUseCaseLanguage(language);
        $scope.useCaseLangage = language;
    };

    $scope.addToVCS = function(useCase) {

        addToVCS(useCase);
        useCase.status = "In VCS";

    };

    $scope.addUseCase = function() {
        addUseCase();
    };

    $scope.editUseCase = function() {

        $scope.editMode = false;
        editUseCase($scope.idUseCaseToEdit);

    };

    $scope.addOrEditUseCase = function() {

        if($scope.editMode) $scope.editUseCase();
        else $scope.addUseCase();

    }

    $scope.showContent = function(content) {
        showContent(content);
    };

    $scope.removeUseCase = function(useCase) {

        removeUseCase(useCase);

        $scope.editMode = false;
        $scope.useCaseLangage = "CSharp";

        clearUseCaseForm();

    };

    $scope.edit = function(useCase) {

        $scope.editMode = true;
        $scope.idUseCaseToEdit = useCase.id;
        $scope.setUseCaseLanguage(useCase.language);

        clearUseCaseForm();
        populateUseCaseForm(useCase);

        $('html,body').scrollTop(0);

    };

    $scope.cancelEditing = function() {

        $scope.editMode = false;
        $scope.useCaseLangage = "CSharp";

        clearUseCaseForm();

    }

});