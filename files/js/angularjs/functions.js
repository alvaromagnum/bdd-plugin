// REFACTOR -> ALL AJAX FUNCTION MUST HAVE THE CURRENT SIGNATURE (BOOL ALON, BOOL SHOW STARTLOADING, BOOL ENDLOADING). REFACTOR FAIL
function addToLog(content, userName) {

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { content: content, userName: userName, option: "addToLog" }

    }).done(function () {

        $.ajax({

            type: "POST",
            url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
            data: { option: "getLogs" }

        }).done(function (obj) {

            if(obj != "") {

                var logs = JSON.parse(obj);

                logsSummaryScope.$apply(function() {

                    logsSummaryScope.logs = [];

                    for(var i in logs) {
                        logsSummaryScope.logs.push(logs[i]);
                    }

                });

            }
            else {

                logsSummaryScope.$apply(function() {
                    logsSummaryScope.logs = [];
                });

            }

        }).fail(function (obj) {

            sweetAlert("Oops...", "Something went wrong!", "error");
            $.unblockUI();

        });

    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function cleanLog() {

    $.blockUI();

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { option: "cleanLog" }

    }).done(function () {

        swal("Good job!", "Success on operation!", "success");
        getLogs(/* alone */ true);

    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function editUseCase(idUseCaseToEdit) {

    $.blockUI();

    var useCaseObject = getUseCaseObject();

    useCaseObject.id = idUseCaseToEdit;

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { useCase: useCaseObject, option: "editUseCase" }

    }).done(function (useCaseAux) {

        clearUseCaseForm();
        getUseCases();

        addToLog("USE CASE UPDATED - " + useCaseObject.title, currentUser);

        swal("Good job!", "Success on operation!", "success");

    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function populateUseCaseForm(useCase) {

    $('#featureTitle').val(useCase.title);
    $('#featureDescription').val(useCase.description);

    $("input[name='useCasePriority'][value='" + useCase.priority + "']").prop('checked', true);

    populateScenarios(useCase);

}

function populateScenarios(useCase) {

    var scenarios = useCase.scenarios;

    var scenarioControl = $('.divScenarioControl').first().clone();

    $('.divScenarioControls').children().remove();

    for (var j in scenarios) {

        var scenarioControlToAdd = scenarioControl.clone();
        var scenario = scenarios[j];
        var recipes = scenario.recipe;
        var gherkinControl = scenarioControlToAdd.find('.divGherkinControl').first().clone();

        $('.divScenarioControls').first().append(scenarioControlToAdd);

        scenarioControlToAdd.find('.divGherkinControls').children().remove();
        scenarioControlToAdd.find('#scenarioDescription').val(scenario.description);

        for (var k in recipes) {

            var gherkinControlToAdd = gherkinControl.clone();
            var recipe = recipes[k];

            scenarioControlToAdd.find('.divGherkinControls').first().append(gherkinControlToAdd);
            gherkinControlToAdd.find('#gherkinType').val(recipe.type);
            gherkinControlToAdd.find('#gherkinRule').val(recipe.rule);

        }

    }

    configureScenarioAddControls();
    configureGherkinAddControls();

}

function showContent(content) {
    $.colorbox({html:"<pre style='white-space: pre-wrap; white-space: -moz-pre-wrap; white-space: -pre-wrap; white-space: -o-pre-wrap; word-wrap: break-word;'>" + content + "</pre>", height:'60%', width:'60%'});
}

function generateAccessors() {

    var interval = setInterval(function(){

        if(configTabScope === undefined || testResultsTabScope === undefined || useCaseTabScope === undefined || resultsSummaryScope === undefined || logsSummaryScope === undefined){

            useCaseTabScope = angular.element(document.getElementById('useCaseTab')).scope();
            configTabScope = angular.element(document.getElementById('configTab')).scope();
            testResultsTabScope = angular.element(document.getElementById('testResultsTab')).scope();
            resultsSummaryScope = angular.element(document.getElementById('resultsSummary')).scope();
            logsSummaryScope = angular.element(document.getElementById('logsSummary')).scope();

        }
        else {

            clearInterval(interval);

        }

    },50);

}

function preparePlugin() {

    generateAccessors();
	
	$("#linkLicense").colorbox({inline:true, width:"80%"});

    $.blockUI();

    getLogs(/* alone */ false);

}

// Root for loading all configurations
function getLogs(alone) {

    if(alone) $.blockUI();

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { option: "getLogs" }

    }).done(function (obj) {

        if(obj != "") {

            var logs = JSON.parse(obj);

            logsSummaryScope.$apply(function() {

                logsSummaryScope.logs = [];

                for(var i in logs) {
                    logsSummaryScope.logs.push(logs[i]);
                }

            });

        }
        else {

            logsSummaryScope.$apply(function() {
                logsSummaryScope.logs = [];
            });

        }

        if(alone) $.unblockUI();
        else getPluginConfig();

    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function getPluginConfig() {

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { option: "getPluginConfig" }

    }).done(function (obj) {

        if(obj != "") {

            var data = JSON.parse(obj);

            configTabScope.$apply(function() {

                configTabScope.svnExePath = data.svnExePath != null ? data.svnExePath : "";
                configTabScope.gitExePath = data.gitExePath != null ? data.gitExePath : "";

            });

        }

        getProjectConfig();

    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function getProjectConfig() {

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { option: "getProjectConfig" }

    }).done(function (obj) {

        if(obj != "") {

            var projectConfig = JSON.parse(obj);

            projectNameSpace = projectConfig.projectNamespace;

            configTabScope.$apply(function() {

                configTabScope.vcsType = projectConfig.vcsType != null ? projectConfig.vcsType : "GIT";
                configTabScope.vcsLocation = projectConfig.vcsLocation != null ? projectConfig.vcsLocation : "Remote";
                configTabScope.vcsUrl = projectConfig.vcsUrl;
                configTabScope.vcsUser = projectConfig.vcsUser != null ? projectConfig.vcsUser : "";
                configTabScope.vcsPassword = projectConfig.vcsPassword != null ? projectConfig.vcsPassword : "";
                configTabScope.projectNamespace = projectConfig.projectNamespace;
                configTabScope.projectFeaturesFolder = projectConfig.featuresFolder;
                configTabScope.projectStepsFolder = projectConfig.stepsFolder;

            });

        }

        getTestResults(/* alone */ false);

    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function getTestResults(alone) {

    if(alone) $.blockUI();

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { option: "getTestResults" }

    }).done(function (obj) {

        if(obj != "") {

            var testResults = JSON.parse(obj);

            testResultsTabScope.$apply(function() {

                testResultsTabScope.testResults = [];
                testResultsTabScope.statusFilter = null;

                for(var i in testResults) {

                    var testResult = testResults[i];

                    switch (testResult.status) {

                        case null:
                            testResult.class = "testResult testResultInconclusive";
                            break;

                        case "1":
                            testResult.class = "testResult testResultSuccess";
                            break;

                        case "0":
                            testResult.class = "testResult testResultFail";
                            break;

                    }

                    testResultsTabScope.testResults.push(testResult);
                }

            });

        }
        else {

            testResultsTabScope.$apply(function() {
                testResultsTabScope.testResults = [];
                testResultsTabScope.statusFilter = null;
            });

        }

        if(alone) $.unblockUI();
        else getUseCases();

    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function getUseCases() {

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { option: "getUseCases" }

    }).done(function (useCasesJson) {

        if(useCasesJson != "") {

            var useCases = JSON.parse(useCasesJson);

            useCaseTabScope.$apply(function() {

                useCaseTabScope.useCases = [];

                for(var i in useCases) {

                    var useCase = useCases[i];

                    useCase.gherkin = getGherkinFromUseCase(useCase);
                    useCase.steps_definition = getStepsDefinitionFromUseCase(useCase, mantisRootUrl);

                    useCaseTabScope.useCases.push(useCase);

                }

            });

            updateResultsSummary(useCases);

        }

        $.unblockUI();


    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function updateResultsSummary(useCases) {

    resultsSummaryScope.$apply(function() {

        var successNumber = 0;
        var failNumber = 0;
        var inconclusiveNumber = 0;
        var unsentNumber = 0;

        for (var i in useCases) {

            var useCase = useCases[i];

            if(useCase.status != "In VCS") {

                unsentNumber++;
                continue;

            }

            var scenarios = useCase.scenarios;

            for (var j in scenarios) {

                var scenario = scenarios[j];

                if(scenario.status == "1") successNumber++;
                else if(scenario.status == "0") failNumber++;
                else inconclusiveNumber++;

            }

        }

        resultsSummaryScope.successNumber = successNumber;
        resultsSummaryScope.failNumber = failNumber;
        resultsSummaryScope.inconclusiveNumber = inconclusiveNumber;
        resultsSummaryScope.unsentNumber = unsentNumber;

    });

}

function clearUseCaseForm() {

    $('.divScenarioControls').children().not(':first').remove();
    $('.divGherkinControls').children().not(':first').remove();

    $('.scenarioFormContainer').find('input:text').val('');

    $("input[name='useCasePriority'][value='Normal']").prop('checked', true);

    setUseCaseLanguage('CSharp');

}

function setUseCaseLanguage(language) {

    $(".project-language").removeClass("selected");

    switch(language) {

        case 'Java':

            $(".project-language-java").addClass("selected");
            break;

        case 'CSharp':

            $(".project-language-csharp").addClass("selected");
            break;

    }

}

function removeUseCase(useCase) {

    $.blockUI();

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { useCaseId: useCase.id, option: "removeUseCase" }

    }).done(function (obj) {

        useCaseTabScope.$apply(function() {
            useCaseTabScope.useCases.splice(useCaseTabScope.useCases.indexOf(useCase), 1);
        });

        getTestResults(/* alone */ true);
        updateResultsSummary(useCaseTabScope.useCases);

        addToLog("USE CASE DELETED - " + useCase.title, currentUser);

        swal("Good job!", "Success on operation!", "success");

    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function saveProjectConfig() {

    $.blockUI();

    projectNameSpace = configTabScope.projectNamespace.trim()

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { vcsType: configTabScope.vcsType.trim(), vcsUrl: configTabScope.vcsUrl.trim(), vcsUser: configTabScope.vcsUser.trim(), vcsPassword: configTabScope.vcsPassword.trim(), projectNamespace: configTabScope.projectNamespace.trim(), projectFeaturesFolder: configTabScope.projectFeaturesFolder.trim(), projectStepsFolder: configTabScope.projectStepsFolder.trim(), vcsLocation: configTabScope.vcsLocation.trim(), option: "setProjectConfig" }

    }).done(function (obj) {

        addToLog("PROJECT CONFIG UPDATED", currentUser);

        swal("Good job!", "Success on operation!", "success");
        $.unblockUI();


    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function savePluginConfig() {

    $.blockUI();

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { svnExePath: configTabScope.svnExePath.trim(), gitExePath: configTabScope.gitExePath.trim(), option: "setPluginConfig" }

    }).done(function (obj) {

        addToLog("PLUGIN CONFIG UPDATED", currentUser);

        swal("Good job!", "Success on operation!", "success");
        $.unblockUI();


    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function addUseCase() {

    $.blockUI();

    var useCaseObject = getUseCaseObject();

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { useCase: useCaseObject, option: "addUseCase" }

    }).done(function (useCaseAux) {

        var useCase = JSON.parse(useCaseAux);

        useCaseObject.gherkin = getGherkinFromUseCase(useCase);
        useCaseObject.steps_definition = getStepsDefinitionFromUseCase(useCase, mantisRootUrl);
        useCaseObject.id = useCase.id;
        useCaseObject.creationDate = useCase.creationDate;

        useCaseTabScope.$apply(function() {
            useCaseTabScope.useCases.push(useCaseObject);
            useCaseTabScope.language = "CSharp";
        });

        updateResultsSummary(useCaseTabScope.useCases);
        clearUseCaseForm();

        addToLog("USE CASE ADDED - " + useCaseObject.title, currentUser);

        swal("Good job!", "Success on operation!", "success");

        $.unblockUI();


    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function getCardClassByPriority(priority) {

    switch(priority) {
        case 'Very Important' : return 'red-card';
        case 'Important' : return 'yellow-card';
        case 'Normal' : return 'grey-card';
    }

}

function addToVCS(useCase) {

    $.blockUI();

    $.ajax({

        type: "POST",
        url: "/"+mantisDir+"/plugin.php?page=BddMantis/rest",
        data: { useCaseId: useCase.id, gherkin: btoa(removeAccents(useCase.gherkin)), stepsDefinition: btoa(removeAccents(useCase.steps_definition)), option: "addToVCS" }

    }).done(function (obj) {

        getTestResults(/* alone */ true);
        updateResultsSummary(useCaseTabScope.useCases);

        addToLog("USE CASE ADDED TO VCS - " + useCase.title, currentUser);

        swal("Good job!", "Success on operation!", "success");

    }).fail(function (obj) {

        sweetAlert("Oops...", "Something went wrong!", "error");
        $.unblockUI();

    });

}

function applyJquery(tab) {

    if(tab == 'testResults') {

    }

}

function configureGherkinAddControls() {

    $( ".addGherkinControl").unbind( "click" );

    $('.addGherkinControl').click(function() {

        var gherkinControlContainer = $($(this).parent().children()[0]);
        var gherkinControlModel = $(gherkinControlContainer.children()[0]);
        var clonedControl = gherkinControlModel.clone();

        clonedControl.children('input').val('');

        gherkinControlContainer.append(clonedControl);

        configureGherkinRemoveControls();

    });

}

function configureGherkinRemoveControls() {

    $( ".removeGherkinControlButton").unbind( "click" );

    $('.removeGherkinControlButton').click(function() {

        if(confirm('Are you sure you want to remove this item?')) {

            var gherkinControlContainer = $($(this).parent().parent().parent().children()[0]);
            if (gherkinControlContainer.children().length > 1) $(this).parent().remove();

        }

    });

}

function configureScenarioAddControls() {

    $( ".addScenarioControl").unbind( "click" );

    $('.addScenarioControl').click(function() {

        var scenarioControlContainer = $($(this).parent().children()[0]);
        var scenarioControlModel = $(scenarioControlContainer.children()[0]);
        var clonedControl = scenarioControlModel.clone();

        clonedControl.children('.divScenarioGherkinControls').children().children().children().children().not(':first').remove();
        clonedControl.children('.divScenarioGherkinControls').children().children().children().children().children('input').val('');
        clonedControl.children().children('input').val('');

        scenarioControlContainer.append(clonedControl);

        configureScenarioRemoveControls();

        configureGherkinAddControls();
        configureGherkinRemoveControls();

    });

}

function configureScenarioRemoveControls() {

    $( ".removeGherkinScenarioButton").unbind( "click" );

    $('.removeGherkinScenarioButton').click(function() {

        if(confirm('Are you sure you want to remove this item?')) {

            var scenarioControlContainer = $($(this).parent().parent().parent());
            if (scenarioControlContainer.children().length > 1) $(this).parent().parent().remove();

        }

    });

}

function configureGherkinControls() {

    configureGherkinAddControls();
    configureGherkinRemoveControls();

}

function configureScenarioControls() {

    configureScenarioAddControls();
    configureScenarioRemoveControls();

    configureGherkinAddControls();
    configureGherkinRemoveControls();

}

function getUseCaseObject() {

    var scenarios = [];

    $('.divScenarioControl').each(function() {

        var rules = [];

        $(this).find(".divGherkinControl").each(function() {

            var recipe = {
                type: $(this).children('select').val(),
                rule: $(this).children('input').val()
            }

            rules.push(recipe);

        });

        var scenario = {
            description: $(this).find("input[name='scenarioDescription']").val(),
            recipe: rules
        };

        scenarios.push(scenario);

    });

    var useCase = {

        title: $('#featureTitle').val(),
        description: $('#featureDescription').val(),
        priority: $("input:radio[name ='useCasePriority']:checked").val(),
        language: useCaseTabScope.useCaseLangage,
        scenarios: scenarios,
        creationDate: (new Date()).toLocaleString(),
        status: "Not in VCS",
        project: "",
        gherkin: null,
        steps_definition: null,
        id: 0

    };

    return useCase;

}

function removeAccents(s){
    var r = s;
    non_asciis = {'a': '[àáâãäå]', 'ae': 'æ', 'c': 'ç', 'e': '[èéêë]', 'i': '[ìíîï]', 'n': 'ñ', 'o': '[òóôõö]', 'oe': 'œ', 'u': '[ùúûűü]', 'y': '[ýÿ]'};
    for (i in non_asciis) { r = r.replace(new RegExp(non_asciis[i], 'g'), i); }
    return r;
}

function getAnnotationFromRule(rule) {

    var newRule = rule.replaceAll(" ", "_").toString();

    var tokens = newRule.split("_");

    for(var i in tokens) {

        var token = tokens[i];

        if(!isNaN(token)) tokens[i] = "(.*)";
        else if(token.startsWith('"') && token.endsWith('"')) tokens[i] = '""(.*)""';
        else if(token.startsWith("'") && token.endsWith("'")) tokens[i] = "'(.*)'";

    }

    newRule = tokens.join(" ");

    return newRule;

}

function getParamsFromRule(rule) {

    var params = [];

    var tokens = rule.split(" ");

    for(var i in tokens) {

        var token = tokens[i];
        var key = params.length;

        if(!isNaN(token)) tokens[i] = params.push("int p" + key);
        else if((token.startsWith('"') && token.endsWith('"')) || (token.startsWith("'") && token.endsWith("'"))) tokens[i] = params.push("string p" + key);

    }

    return params.join(", ");

}

function getMethodNameFromRule(rule) {

    var newRule = rule;

    var tokens = newRule.split(" ");

    for(var i in tokens) {

        var token = tokens[i];

        if(!isNaN(token) || (token.startsWith('"') && token.endsWith('"')) || (token.startsWith("'") && token.endsWith("'"))) tokens[i] = "";

    }

    newRule = tokens.join(" ").collapseWhitespace().toString().trim();

    return newRule;

}

function getGherkinFromUseCase(useCase) {

    S.extendPrototype();

    var gherkin;
    var gherkinTemplate = "{{header}}\n\n{{scenarios}}";
    var headerTemplate = "Feature: {{title}}\n\t{{description}}";
    var scenarioTemplate = "\t@{{scenarioId}}\n\tScenario: {{title}}\n{{recipe}}";
    var ruleTemplate = "\t\t{{type}} {{rule}}"
    var headerValues = {title: useCase.title, description: useCase.description};
    var header = headerTemplate.template(headerValues).toString();

    var scenarios = "";

    for(var i in useCase.scenarios) {

        var scenarioObj = useCase.scenarios[i];
        var recipe = "";

        for(var j in scenarioObj.recipe) {

            var ruleObj = scenarioObj.recipe[j];
            var ruleValues = {type: ruleObj.type, rule: ruleObj.rule};

            recipe += ruleTemplate.template(ruleValues).toString() + '\n';

        }

        var scenarioValues = {scenarioId: scenarioObj.id,title: scenarioObj.description, recipe: recipe};

        scenarios += scenarioTemplate.template(scenarioValues).toString() + '\n';

    }

    var gherkinValues = {header: header, scenarios: scenarios};

    gherkin = gherkinTemplate.template(gherkinValues).toString();

    S.restorePrototype();

    return gherkin;

}

function getStepsComunicationCode(mantisRootUrl) {

    var comunicationCodeT = "\t\t#region BddPluginComunication\n\n";

    comunicationCodeT += '\t\t// ------------------------------------------------------------------------------\n';
    comunicationCodeT += '\t\t//  <auto-generated>\n';
    comunicationCodeT += '\t\t//      FROM THIS POINT: This code was generated by BDD Plugin for Mantis 1.0\n';
    comunicationCodeT += '\t\t//\n';
    comunicationCodeT += '\t\t//      Changes to this file may cause incorrect behavior and will be lost if\n';
    comunicationCodeT += '\t\t//      the code is regenerated.\n';
    comunicationCodeT += '\t\t//  </auto-generated>\n';
    comunicationCodeT += '\t\t// ------------------------------------------------------------------------------\n\n';
    comunicationCodeT += '\t\t[AfterScenario]\n';
    comunicationCodeT += '\t\tpublic void AfterScenario()\n';
    comunicationCodeT += '\t\t{\n\n';
    comunicationCodeT += '\t\t\tvar error = ScenarioContext.Current.TestError;\n';
    comunicationCodeT += '\t\t\tvar id = ScenarioContext.Current.ScenarioInfo.Tags[0];\n\n';
    comunicationCodeT += '\t\t\tstring errorMessage = null;\n';
    comunicationCodeT += '\t\t\tstring status = null;\n\n';
    comunicationCodeT += '\t\t\tstring url = "{{mantisRootUrl}}plugin.php?page=BddMantis/rest&option=updateScenarioTest&idScenario={0}&status={1}&errorMessage={2}";\n\n';
    comunicationCodeT += '\t\t\tif (error == null)\n';
    comunicationCodeT += '\t\t\t{\n';
    comunicationCodeT += '\t\t\t\tstatus = "1";\n';
    comunicationCodeT += '\t\t\t}\n';
    comunicationCodeT += '\t\t\telse\n';
    comunicationCodeT += '\t\t\t{\n';
    comunicationCodeT += '\t\t\t\terrorMessage = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(error.Message));\n';
    comunicationCodeT += '\t\t\t\tstatus = "0";\n';
    comunicationCodeT += '\t\t\t}\n\n';
    comunicationCodeT += '\t\t\tWebRequest.Create(string.Format(url, id, status, errorMessage)).GetResponse();\n\n';
    comunicationCodeT += '\t\t}\n\n';
    comunicationCodeT += '\t\t#endregion\n';

    var comunicationCodeValues = {mantisRootUrl: mantisRootUrl};
    var comunicationCode = comunicationCodeT.template(comunicationCodeValues).toString();

    return comunicationCode;

}

function getStepsDefinitionFromUseCase(useCase, mantisRootUrl) {

    S.extendPrototype();

    var className = removeAccents(useCase.title);

    className = className.replaceAll(" ", "_").toString();
    className = className.capitalize().toString();
    className = className.camelize().toString();

    var stepsT = "{{header}}\n\n{{methods}}\n\n{{generatedCode}}\n\n{{footer}}";
    var headerT = "namespace {{nameSpace}}\n{\n\n\tusing System;\n\tusing System.Net;\n\n\tusing TechTalk.SpecFlow;\n\n\t[Binding]\n\tclass {{className}}Steps\n\t{";
    var generatedCode = getStepsComunicationCode(mantisRootUrl);
    var footer = "\t}\n\n}";
    var headerValues = {nameSpace: projectNameSpace, className: className};
    var header = headerT.template(headerValues).toString();

    var methods = "";

    var methodsControl = [];

    for(var i in useCase.scenarios) {

        var scenarioObj = useCase.scenarios[i];

        for(var j in scenarioObj.recipe) {

            var ruleObj = scenarioObj.recipe[j];

            var type = ruleObj.type;
            var rule = ruleObj.rule.collapseWhitespace().toString();

            if(type == "And") type = "Given";

            var methodName = removeAccents(rule);

            methodName = getMethodNameFromRule(methodName);
            methodName = methodName.replaceAll(" ", "_").toString();
            methodName = methodName.capitalize().toString();
            methodName = type + "_" + methodName;
            methodName = methodName.camelize().toString();

            var annotationT = '[{{type}}(@"{{rule}}")]';
            var annotationValues = {type: type, rule: getAnnotationFromRule(rule)};
            var annotation = annotationT.template(annotationValues).toString();

            var signatureT = "public void {{methodName}}({{params}})";
            var signatureValues = {methodName: methodName, params: getParamsFromRule(rule)};
            var signature = signatureT.template(signatureValues).toString();

            if($.inArray(signature, methodsControl) != -1) continue;
            else(methodsControl.push(signature));

            var content = "\t\t\tScenarioContext.Current.Pending();";

            var methodT = "\t\t{{annotation}}\n\t\t{{signature}}\n\t\t{\n{{content}}\n\t\t}\n\n";
            var methodValues = {annotation: annotation, signature: signature, content: content};
            var method = methodT.template(methodValues).toString();

            methods += method;

        }

    }

    var stepsValues = {header: header, methods: methods, generatedCode: generatedCode, footer: footer};
    var steps_definition = stepsT.template(stepsValues).toString();

    S.restorePrototype();

    return steps_definition;

}