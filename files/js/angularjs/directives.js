bddPluginApp.directive('homeTab', function() {
    return {
        restrict: 'E',
        templateUrl: 'plugins/BddMantis/files/templates/homeTab.html'
    }
});

bddPluginApp.directive('configTab', function() {
    return {
        restrict: 'E',
        templateUrl: 'plugins/BddMantis/files/templates/configTab.html'
    }
});

bddPluginApp.directive('useCaseInfoCard', function() {
    return {
        restrict: 'E',
        templateUrl: 'plugins/BddMantis/files/templates/useCaseInfoCard.html'
    }
});

bddPluginApp.directive('resultsSummary', function() {
    return {
        restrict: 'E',
        templateUrl: 'plugins/BddMantis/files/templates/resultsSummary.html'
    }
});

bddPluginApp.directive('newUseCaseForm', function() {
    return {
        restrict: 'E',
        templateUrl: 'plugins/BddMantis/files/templates/newUseCaseForm.html'
    }
});

bddPluginApp.directive('testResultsTab', function() {
    return {
        restrict: 'E',
        templateUrl: 'plugins/BddMantis/files/templates/testResultsTab.html'
    }
});

bddPluginApp.directive('gherkinCreatorForm', function() {
    return {
        restrict: 'E',
        templateUrl: 'plugins/BddMantis/files/templates/gherkinCreatorForm.html',
        link: function ($scope, element, attrs) {
            configureGherkinControls();
        }
    }
});

bddPluginApp.directive('scenarioCreatorForm', function() {
    return {
        restrict: 'E',
        templateUrl: 'plugins/BddMantis/files/templates/scenarioCreatorForm.html',
        link: function ($scope, element, attrs) {
            configureScenarioControls();
        }
    }
});

bddPluginApp.directive('useCasesTab', function() {
    return {
        restrict: 'E',
        templateUrl: 'plugins/BddMantis/files/templates/useCasesTab.html',
        link: function ($scope, element, attrs) {
            $('[title]').qtip();
        }
    }
});