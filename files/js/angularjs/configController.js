bddPluginApp.controller('ConfigController', function ($scope) {

    $scope.gitExePath = "";
    $scope.svnExePath = "";

    $scope.vcsType = "GIT";
    $scope.vcsLocation = "Remote";
    $scope.vcsUrl = "";
    $scope.vcsUser = "";
    $scope.vcsPassword = "";
    $scope.projectNamespace = "";
    $scope.projectFeaturesFolder = "";
    $scope.projectStepsFolder = "";

    $scope.savePluginConfig = function() {
        savePluginConfig();
    };

    $scope.saveProjectConfig = function() {
        saveProjectConfig();
    };

});