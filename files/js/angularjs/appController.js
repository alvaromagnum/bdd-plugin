bddPluginApp.controller('AppController', function ($scope) {

    $scope.tab = "home";

    $scope.setTab = function(newTab) {

        $scope.tab = newTab;
        applyJquery($scope.tab);

    };

    $scope.selectedTab = function(selectedTab) {
        return $scope.tab == selectedTab;
    };

    $scope.showContent = function(content) {
        showContent(content);
    };

    $scope.loadHome = function() {

        $scope.tab = "home";

        $.blockUI();

        getUseCases(); // REFACTOR. WORKAROUND TO LOAD RESULTS SUMMARY.

    }

    $scope.loadTestResults = function() {

        $scope.tab = "testResults";
        getTestResults(/* alone */ true); // REFATOR. LOAD SHOULD BE HERE. REMOVE FROM OTHER PLACES

    }

});