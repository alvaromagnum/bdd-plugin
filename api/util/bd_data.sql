INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_table` (`id`,`title`,`description`,`priority`,`language`,`creation_date`,`status`,`project_id`) VALUES (1,'Calculadora','Calculadora digital para somar, subtrair, multiplicar e dividir 2 números','Normal','JAVA','2014/01/01','Sent',1);

INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_scenario_table` (`id`,`description`,`usecase_id`) VALUES (1,'Soma de 2 números',1);
INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_scenario_table` (`id`,`description`,`usecase_id`) VALUES (2,'Multiplicação de 2 números',1);

INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_scenario_recipe_table` (`id`,`type`,`rule`,`scenario_id`) VALUES (1,'Given','que o número A é 10',1);
INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_scenario_recipe_table` (`id`,`type`,`rule`,`scenario_id`) VALUES (2,'And','o número B é 20',1);
INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_scenario_recipe_table` (`id`,`type`,`rule`,`scenario_id`) VALUES (3,'When','a soma for efetuada',1);
INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_scenario_recipe_table` (`id`,`type`,`rule`,`scenario_id`) VALUES (4,'Then','o resultado deve ser 30',1);
INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_scenario_recipe_table` (`id`,`type`,`rule`,`scenario_id`) VALUES (5,'Given','que o número A é 2',2);
INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_scenario_recipe_table` (`id`,`type`,`rule`,`scenario_id`) VALUES (6,'And','o número B é 3',2);
INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_scenario_recipe_table` (`id`,`type`,`rule`,`scenario_id`) VALUES (7,'When','a multiplicação for feita',2);
INSERT INTO `mantis_plugin_bddmantis_bdd_use_case_scenario_recipe_table` (`id`,`type`,`rule`,`scenario_id`) VALUES (8,'Then','o resultado deve ser 6',2);
