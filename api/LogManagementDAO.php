<?php

class LogManagementDAO {

    static function addToLog($content, $userName) {

        $log = new Log();

        $log->description = $content;
        $log->user_name = $userName;
        $log->date = date("Y-m-d H:i:s");

        $log->save();

    }

    static function cleanLog() {
        Log::all()->destroy();
    }

    static function getLogs() {
        return Log::all();
    }

}