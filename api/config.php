<?php

$con = new PDO('mysql:host=localhost;dbname=bugtracker;charset=utf8', 'root', 'vertrigo');
$con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_SILENT);

TORM\Connection::setConnection($con);

UseCase::setTableName(plugin_table($tableUseCase));
UseCase::hasMany("scenarios", array("class_name"=>"Scenario"));

Scenario::setTableName(plugin_table($tableScenario));
Scenario::hasMany("recipes", array("class_name"=>"Recipe"));
Scenario::belongsTo("useCase", array("class_name"=>"UseCase"));

Recipe::setTableName(plugin_table($tableRecipe));
Recipe::belongsTo("scenario", array("class_name"=>"Scenario"));

PluginConfig::setTableName(plugin_table($tableConfiguration));

ProjectConfig::setTableName(plugin_table($tableProjectsConfiguration));

Log::setTableName(plugin_table($tableLog));