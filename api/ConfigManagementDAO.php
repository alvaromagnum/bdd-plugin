<?php

class ConfigManagementDAO {

    static function getPluginConfig() {

        $svnExePath = PluginConfig::where(array("param"=> "svnExePath"))->next()->value;
        $gitExePath = PluginConfig::where(array("param"=> "gitExePath"))->next()->value;

        return array("svnExePath" => $svnExePath, "gitExePath" => $gitExePath);

    }

    static function getProjectConfig($projectId) {
        return ProjectConfig::where(array("project_id" => $projectId))->next();
    }

    static function setPluginConfig($gitExePath, $svnExePath) {

        PluginConfig::where(array("param"=> "svnExePath"))->updateAttributes(array("value"=> $svnExePath == "" ? null : $svnExePath));
        PluginConfig::where(array("param"=> "gitExePath"))->updateAttributes(array("value"=> $gitExePath == "" ? null : $gitExePath));

    }

    static function setProjectConfig($vcsType, $vcsUrl, $vcsUser, $vcsPassword, $projectNamespace, $projectFeaturesFolder, $projectStepsFolder, $projectId, $vcsLocation) {

        $configCollection = ProjectConfig::where(array("project_id"=> $projectId));

        if($configCollection->count() > 0) {

            $configCollection->updateAttributes(array("vcs_type"=> $vcsType, "vcs_url"=> $vcsUrl, "vcs_passwd"=> $vcsPassword, "project_namespace"=> $projectNamespace, "features_folder"=> $projectFeaturesFolder, "steps_folder"=> $projectStepsFolder, "vcs_user"=> $vcsUser, "vcs_location" => $vcsLocation));

        }
        else {

            $projectConfig = new ProjectConfig();

            $projectConfig->vcs_type = $vcsType;
            $projectConfig->vcs_url = $vcsUrl;
            $projectConfig->vcs_passwd = $vcsPassword;
            $projectConfig->project_namespace = $projectNamespace;
            $projectConfig->features_folder = $projectFeaturesFolder;
            $projectConfig->steps_folder = $projectStepsFolder;
            $projectConfig->vcs_user = $vcsUser;
            $projectConfig->vcs_location = $vcsLocation;
            $projectConfig->project_id = $projectId;

            $projectConfig->save();

        }

    }

}