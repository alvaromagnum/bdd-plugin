<?php

class UseCaseObj {

    public $id;
    public $title;
    public $description;
    public $priority;
    public $language;
    public $scenarios;
    public $creationDate;
    public $status;
    public $project;
    public $gherkin;
    public $steps_definition;

    function __construct() {

        $this->scenarios = array();

        $this->id = 0;

        $this->title = "";
        $this->description = "";
        $this->priority = "";
        $this->language = "";
        $this->creationDate = "";
        $this->status = "";
        $this->project = "";
        $this->gherkin = "";
        $this->steps_definition = "";

    }

    function addScenario($scenario) {
        array_push($this->scenarios, $scenario);
    }

} 