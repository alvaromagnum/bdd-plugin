<?php

class ScenarioObj {

    public $id;
    public $description;
    public $recipe;
    public $status;
    public $errorMessage;
    public $featureTitle;
    public $class;

    function __construct() {

        $this->recipe = array();

        $this->description = "";
        $this->featureTitle = "";
        $this->class = "";

        $this->status = null;
        $this->errorMessage = null;

        $this->id = 0;

    }

    function addRecipe($recipe) {
        array_push($this->recipe, $recipe);
    }

} 