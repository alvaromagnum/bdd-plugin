<?php


class ProjectConfigObj {

    public $id;
    public $vcsUrl;
    public $vcsUser;
    public $vcsLocation;
    public $vcsPassword;
    public $featuresFolder;
    public $vcsType;
    public $stepsFolder;
    public $projectNamespace;
    public $projectId;

    function __construct() {

        $this->id = "";
        $this->vcsUrl = "";
        $this->vcsUser = "";
        $this->vcsLocation = "";
        $this->vcsPassword = "";
        $this->featuresFolder = "";
        $this->vcsType = "";
        $this->stepsFolder = "";
        $this->projectNamespace = "";
        $this->projectId = "";

    }

} 