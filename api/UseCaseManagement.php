<?php

class UseCaseManagement {

    static function getUseCases($projectId) {

        $useCases = UseCaseManagementDAO::getUseCases($projectId);

        $result = array();

        if($useCases->count() == 0) return "";

        foreach($useCases as $useCase) {
            array_push($result, self::createUseCaseObjectFromModel($useCase));
        }

        return json_encode($result);

    }

    static function removeUseCase($useCaseId) {
        UseCaseManagementDAO::removeUseCase($useCaseId);
    }

    static function getTestResults($projectId) {

        $useCases = UseCaseManagementDAO::getUseCasesWithTest($projectId);

        $useCasesObjects = array();
        $result = array();

        if($useCases->count() == 0) return "";

        foreach($useCases as $useCase) {
            array_push($useCasesObjects, self::createUseCaseObjectFromModel($useCase));
        }

        foreach($useCasesObjects as $useCaseObject) {

            $scenarios = $useCaseObject->scenarios;

            foreach($scenarios as $scenario) {
                $scenario->recipe = null;
                array_push($result, $scenario);
            }

        }

        if(count($result) == 0) return "";

        return json_encode($result);

    }

    static function addUseCase($useCase, $projectId) {

        $useCaseId = UseCaseManagementDAO::addUseCase($useCase, $projectId);
        $useCaseModel = UseCaseManagementDAO::getUseCase($useCaseId);

        return json_encode(self::createUseCaseObjectFromModel($useCaseModel));

    }

    static function updateScenarioTest($idScenario, $status, $errorMessage) {
        UseCaseManagementDAO::updateScenarioTest($idScenario, $status, $errorMessage);
    }

    private static function stripAccents($str) {
        return strtr(utf8_decode($str), utf8_decode('àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ'), 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY');
    }

    static function addToVCS($useCaseId, $gherkin, $stepsDefinition) {

        UseCaseManagementDAO::updateGherkinAndSteps($useCaseId, $gherkin, $stepsDefinition);

        $useCase = UseCaseManagementDAO::getUseCase($useCaseId);

        $featureName = str_replace(" ", "", ucwords(strtolower(self::stripAccents($useCase->title))));
        $featureFileName = "$featureName.feature";
        $stepsFileName = $featureName."Steps.cs";

        $projectId = $useCase->project_id;

        $projectConfig = ConfigManagementDAO::getProjectConfig($projectId);

        $rootPath = dirname(dirname(__FILE__));

        $reposPath = "$rootPath\\repos";
        $projectDir = "$reposPath\\$projectId";


        @mkdir($projectDir);

        $user = $projectConfig->vcs_user;
        $password = $projectConfig->vcs_passwd;

        switch($projectConfig->vcs_type) {

            case "GIT":

                $data = ConfigManagementDAO::getPluginConfig();

                Git::set_bin("\"$data[gitExePath]\"");

                if($projectConfig->vcs_location == "Local") {

                    $repo = Git::open($projectConfig->vcs_url);

                    $featuresDir = "$projectConfig->vcs_url\\$projectConfig->features_folder";
                    $stepsDir = "$projectConfig->vcs_url\\$projectConfig->steps_folder";

                }
                else {

                    $featuresDir = "$projectDir\\$projectConfig->features_folder";
                    $stepsDir = "$projectDir\\$projectConfig->steps_folder";

                    try {

                        $repo = Git::open($projectDir);

                    }
                    catch(Exception $ex) {

                        $connectionUrl = "https://$user:$password@".str_replace("https://", "", $projectConfig->vcs_url);
                        $repo = Git::clone_remote($projectDir, $connectionUrl);

                    }

                }

                $featureFilePath = "$featuresDir\\$featureFileName";
                $stepsFilePath = "$stepsDir\\$stepsFileName";

                if(!file_exists($featureFilePath)) {

                    file_put_contents($featureFilePath, $gherkin);
                    $repo->add($featureFilePath);

                }

                if(!file_exists($stepsFilePath)) {

                    file_put_contents($stepsFilePath, $stepsDefinition);
                    $repo->add($stepsFilePath);

                }

                file_put_contents($featureFilePath, $gherkin);
                file_put_contents($stepsFilePath, $stepsDefinition);

                $useCase->updateAttributes(array("status"=> "In VCS"));

                $repo->commit('AddingBDDTests');
                $repo->push('origin', 'master');

                break;

        }

    }

    private static function createUseCaseObjectFromModel($useCase) {

        $useCaseObj = new UseCaseObj();

        $useCaseObj->id  = $useCase->id;
        $useCaseObj->title  = $useCase->title;
        $useCaseObj->description = $useCase->description;
        $useCaseObj->priority = $useCase->priority;
        $useCaseObj->language = $useCase->language;
        $useCaseObj->creationDate = $useCase->creation_date;
        $useCaseObj->status = $useCase->status;
        $useCaseObj->gherkin = $useCase->gherkin;
        $useCaseObj->steps_definition = $useCase->steps_definition;
        $useCaseObj->projectId = $useCase->project_id;

        foreach($useCase->scenarios as $scenario) {

            $scenarioObj = new ScenarioObj();

            $scenarioObj->id = $scenario->id;
            $scenarioObj->description = $scenario->description;
            $scenarioObj->featureTitle = $useCase->title;
            $scenarioObj->status = $scenario->status;
            $scenarioObj->errorMessage = $scenario->error_message;

            foreach($scenario->recipes as $recipe) {

                $recipeObj = new RecipeObj();

                $recipeObj->id = $recipe->id;
                $recipeObj->rule = $recipe->rule;
                $recipeObj->type = $recipe->type;

                $scenarioObj->addRecipe($recipeObj);

            }

            $useCaseObj->addScenario($scenarioObj);

        }

        return $useCaseObj;

    }

} 