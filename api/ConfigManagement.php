<?php

class ConfigManagement {

    static function getPluginConfig() {

        $data = ConfigManagementDAO::getPluginConfig();

        $pluginConfigObject = new PluginConfigObj();

        $pluginConfigObject->gitExePath = $data["gitExePath"];
        $pluginConfigObject->svnExePath = $data["svnExePath"];

        return json_encode($pluginConfigObject);

    }

    static function getProjectConfig($projectId) {

        $projectConfig = ConfigManagementDAO::getProjectConfig($projectId);

        $projectConfigObject = new ProjectConfigObj();

        $projectConfigObject->id = $projectConfig->id;
        $projectConfigObject->vcsUrl = $projectConfig->vcs_url;
        $projectConfigObject->vcsUser = $projectConfig->vcs_user;
        $projectConfigObject->vcsPassword = $projectConfig->vcs_passwd;
        $projectConfigObject->featuresFolder = $projectConfig->features_folder;
        $projectConfigObject->vcsType = $projectConfig->vcs_type;
        $projectConfigObject->stepsFolder = $projectConfig->steps_folder;
        $projectConfigObject->projectNamespace = $projectConfig->project_namespace;
        $projectConfigObject->projectId = $projectConfig->project_id;
        $projectConfigObject->vcsLocation = $projectConfig->vcs_location;

        return json_encode($projectConfigObject);

    }

    static function setPluginConfig($gitExePath, $svnExePath) {
        ConfigManagementDAO::setPluginConfig($gitExePath, $svnExePath);
    }

    static function setProjectConfig($vcsType, $vcsUrl, $vcsUser, $vcsPassword, $projectNamespace, $projectFeaturesFolder, $projectStepsFolder, $projectId, $vcsLocation) {
        ConfigManagementDAO::setProjectConfig($vcsType, $vcsUrl, $vcsUser, $vcsPassword, $projectNamespace, $projectFeaturesFolder, $projectStepsFolder, $projectId, $vcsLocation);
    }

} 