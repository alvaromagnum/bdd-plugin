<?php

class UseCaseManagementDAO {

    static function getUseCases($projectId) {
        return UseCase::where(array("project_id"=> $projectId));
    }

    static function getUseCasesWithTest($projectId) {
        return UseCase::where(array("project_id"=> $projectId, "status"=>"In VCS"));
    }

    static function removeUseCase($useCaseId) {

        $useCase = UseCase::find($useCaseId);
        $scenarios = $useCase->scenarios;

        foreach($scenarios as $scenario) {
            $recipes = $scenario->recipes;
            $recipes->destroy();
        }

        $scenarios->destroy();
        $useCase->destroy();

    }

    static function getUseCase($useCaseId) {
        return UseCase::find($useCaseId);
    }

    static function updateScenarioTest($idScenario, $status, $errorMessage) {
        $scenario = Scenario::find($idScenario);
        $scenario->updateAttributes(array("status"=>$status, "error_message"=>$errorMessage));
    }

    static function updateGherkinAndSteps($useCaseId, $gherkin, $stepsDefinition) {
        $useCase = UseCase::find($useCaseId);
        $useCase->updateAttributes(array("gherkin"=>$gherkin, "steps_definition"=>$stepsDefinition));
    }

    static function addUseCase($useCaseObj, $projectId) {

        $useCase = new UseCase();

        $useCase->title  = $useCaseObj->title;
        $useCase->description = $useCaseObj->description;
        $useCase->priority = $useCaseObj->priority;
        $useCase->language = $useCaseObj->language;
        $useCase->creation_date = date("Y-m-d H:i:s");
        $useCase->status = $useCaseObj->status;
        $useCase->project_id = $projectId;

        foreach($useCaseObj->scenarios as $scenarioObject) {

            $scenario = new Scenario();
            $scenario->description = $scenarioObject->description;

            foreach($scenarioObject->recipe as $recipeObject) {

                $recipe = new Recipe();

                $recipe->rule = $recipeObject->rule;
                $recipe->type = $recipeObject->type;

                $scenario->push($recipe);

            }

            $useCase->push($scenario);

        }

        $useCase->save();

        return $useCase->id;

    }

}