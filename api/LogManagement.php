<?php

class LogManagement {

    static function addToLog($content, $userName) {
        LogManagementDAO::addToLog($content, $userName);
    }

    static function getLogs() {

        $logs = LogManagementDAO::getLogs();

        $result = array();

        if($logs->count() == 0) return "";

        foreach($logs as $log) {
            array_push($result, self::createLogObjectFromModel($log));
        }

        return json_encode($result);

    }

    static function cleanLog() {
        LogManagementDAO::cleanLog();
    }

    private static function createLogObjectFromModel($log) {

        $logObj = new LogObj();

        $logObj->date = $log->date;
        $logObj->description = $log->description;
        $logObj->userName = $log->user_name;

        return $logObj;

    }

} 