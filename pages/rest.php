<?php

$option = @$_REQUEST['option'];

switch($option) {

    case "addUseCase":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        $projectId = $g_cache_current_project;
        $useCase = json_decode(json_encode($_REQUEST['useCase']));

        echo event_signal('EVENT_BDD_PLUGIN_ADD_USE_CASE', array(array("useCase" => $useCase, "projectId" => $projectId)));

        break;

    case "getLogs":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        echo event_signal('EVENT_BDD_PLUGIN_GET_LOGS');

        break;

    case "cleanLog":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        event_signal('EVENT_BDD_PLUGIN_CLEAN_LOG');

        break;

    case "editUseCase":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        $projectId = $g_cache_current_project;
        $useCase = json_decode(json_encode($_REQUEST['useCase']));

        echo event_signal('EVENT_BDD_PLUGIN_EDIT_USE_CASE', array(array("useCase" => $useCase, "projectId" => $projectId)));

        break;

    case "getUseCases":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        $projectId = $g_cache_current_project;

        echo event_signal('EVENT_BDD_PLUGIN_GET_USE_CASES', $projectId);

        break;

    case "removeUseCase":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        $useCaseId = $_REQUEST['useCaseId'];

        echo event_signal('EVENT_BDD_PLUGIN_REMOVE_USE_CASES', $useCaseId);

        break;

    case "getPluginConfig":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        echo event_signal('EVENT_BDD_PLUGIN_GET_PLUGIN_CONFIG');

        break;

    case "getProjectConfig":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        $projectId = $g_cache_current_project;

        echo event_signal('EVENT_BDD_PLUGIN_GET_PROJECT_CONFIG', $projectId);

        break;

    case "getTestResults":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        $projectId = $g_cache_current_project;

        echo event_signal('EVENT_BDD_PLUGIN_GET_TEST_RESULTS', $projectId);

        break;

    case "setPluginConfig":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        $gitExePath = $_REQUEST['gitExePath'];
        $svnExePath = $_REQUEST['svnExePath'];

        event_signal('EVENT_BDD_PLUGIN_SET_PLUGIN_CONFIG', array(array("gitExePath" => $gitExePath, "svnExePath" => $svnExePath)));

        break;

    case "addToLog":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        $content = $_REQUEST['content'];
        $userName = $_REQUEST['userName'];

        event_signal('EVENT_BDD_PLUGIN_ADD_TO_LOG', array(array("content" => $content, "userName" => $userName)));

        break;

    case "setProjectConfig":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        $projectId = $g_cache_current_project;
        $vcsType = $_REQUEST['vcsType'];
        $vcsUrl = $_REQUEST['vcsUrl'];
        $vcsUser = $_REQUEST['vcsUser'];
        $vcsPassword = $_REQUEST['vcsPassword'];
        $projectNamespace = $_REQUEST['projectNamespace'];
        $projectFeaturesFolder = $_REQUEST['projectFeaturesFolder'];
        $projectStepsFolder = $_REQUEST['projectStepsFolder'];
        $vcsLocation = $_REQUEST['vcsLocation'];

        event_signal('EVENT_BDD_PLUGIN_SET_PROJECT_CONFIG', array(array("vcsType" => $vcsType, "vcsUrl" => $vcsUrl, "vcsUser" => $vcsUser, "vcsPassword" => $vcsPassword, "projectNamespace" => $projectNamespace, "projectFeaturesFolder" => $projectFeaturesFolder, "projectStepsFolder" => $projectStepsFolder, "vcsLocation" => $vcsLocation, "projectId" => $projectId)));

        break;

    case "addToVCS":

        access_ensure_global_level( plugin_config_get( 'rest_services' ) );

        $useCaseId = $_REQUEST['useCaseId'];
        $gherkinBase64 = $_REQUEST['gherkin'];
        $stepsDefinitionBase64 = $_REQUEST['stepsDefinition'];

        event_signal('EVENT_BDD_PLUGIN_GET_ADD_TO_VCS', array(array("useCaseId" => $useCaseId, "gherkinBase64" => $gherkinBase64, "stepsDefinitionBase64" => $stepsDefinitionBase64)));

        break;

    case "updateScenarioTest":

        $idScenario = $_REQUEST['idScenario'];
        $status = $_REQUEST['status'];
        $errorMessageBase64 = $_REQUEST['errorMessage'];

        event_signal('EVENT_BDD_PLUGIN_UPDATE_SCENARIO_TEST', array(array("idScenario" => $idScenario, "status" => $status, "errorMessageBase64" => $errorMessageBase64)));

        break;

}