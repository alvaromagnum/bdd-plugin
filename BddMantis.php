<?php

class BddMantisPlugin extends MantisPlugin {

    function register() {

        $this->name = 'Mantis BDD Plugin';                                           # Proper name of plugin
        $this->description = 'BDD Plugin for Mantis';                                # Short description of the plugin
        $this->page = 'index';                                                       # Default plugin page
        $this->version = '1.0';                                                      # Plugin version string

        $this->requires = array(                                                     # Plugin dependencies, array of basename => version pairs
            'MantisCore' => '1.2.0',                                                 # Should always depend on an appropriate version of MantisBT
        );

        $this->author = 'Vinicius Cardoso Garcia, Alvaro Magnum Barbosa Neto';       # Author/team name
        $this->contact = 'ambn@cin.ufpe.br';                                         # Author/team e-mail address
        $this->url = '#';                                                            # Support webpage

    }

    function schema() {

        include 'api/inc.php';

        $t_mantis_bdd_use_case_table = plugin_table($tableUseCase);
        $t_mantis_bdd_use_case_scenario_table = plugin_table($tableScenario);
        $t_mantis_bdd_scenario_recipe_table = plugin_table($tableRecipe);
        $t_mantis_bdd_projects_configuration_table = plugin_table($tableProjectsConfiguration);
        $t_mantis_bdd_configuration_table = plugin_table($tableConfiguration);
        $t_mantis_bdd_log_table = plugin_table($tableLog);

        return array(

            // version 1.0.0
            array("CreateTableSQL", array($t_mantis_bdd_use_case_table, "
					id INT NOTNULL AUTOINCREMENT PRIMARY,
                    title VARCHAR(50) NOTNULL,
                    description TEXT NOTNULL,
                    priority VARCHAR(20) NOTNULL,
                    language VARCHAR(10) NOTNULL,
                    creation_date DATETIME NOTNULL,
                    status VARCHAR(10) NOTNULL,
                    gherkin TEXT,
                    steps_definition TEXT,
                    project_id INT NOTNULL
            ")),

            array("CreateTableSQL", array($t_mantis_bdd_use_case_scenario_table, "
					id INT NOTNULL AUTOINCREMENT PRIMARY,
                    description TEXT NOTNULL,
                    status INT,
                    error_message TEXT,
                    usecase_id INT NOTNULL
            ")),

            array("CreateTableSQL", array($t_mantis_bdd_scenario_recipe_table, "
					id INT NOTNULL AUTOINCREMENT PRIMARY,
                    type VARCHAR(10) NOTNULL,
                    rule TEXT NOTNULL,
                    scenario_id INT NOTNULL
            ")),

            array("CreateTableSQL", array($t_mantis_bdd_projects_configuration_table, "
					id INT NOTNULL AUTOINCREMENT PRIMARY,
                    vcs_url VARCHAR(100) NOTNULL,
                    vcs_location VARCHAR(10) NOTNULL,
                    vcs_user VARCHAR(50),
                    vcs_passwd VARCHAR(50),
                    features_folder VARCHAR(50) NOTNULL,
                    vcs_type VARCHAR(3) NOTNULL,
                    steps_folder VARCHAR(50) NOTNULL,
                    project_namespace VARCHAR(100) NOTNULL,
                    project_id INT NOTNULL
            ")),

            array("CreateTableSQL", array($t_mantis_bdd_configuration_table, "
                    param VARCHAR(50) NOTNULL PRIMARY,
	                value VARCHAR(100)
            ")),

            array("CreateTableSQL", array($t_mantis_bdd_log_table, "
                    id INT NOTNULL AUTOINCREMENT PRIMARY,
                    date DATETIME NOTNULL,
                    user_name VARCHAR(50) NOTNULL,
	                description TEXT NOTNULL
            ")),

            array("CreateTableSQL", array("torm_info", "
					id INT
            ")),

            array("InsertData", array($t_mantis_bdd_configuration_table, "
					(param, value) VALUES ('gitExePath', null)
            ")),

            array("InsertData", array($t_mantis_bdd_configuration_table, "
					(param, value) VALUES ('svnExePath', null)
            ")),

        );

    }

    function config() {
        return array(
            'rest_services' => ADMINISTRATOR,
            'bdd_app' => ADMINISTRATOR,
        );
    }

    function init() {

        include 'api/inc.php';

        require_once 'api/git/Git.php';

        require_once 'api/models/UseCaseObj.php';
        require_once 'api/models/ScenarioObj.php';
        require_once 'api/models/RecipeObj.php';
        require_once 'api/models/PluginConfigObj.php';
        require_once 'api/models/ProjectConfigObj.php';
        require_once 'api/models/LogObj.php';

        require_once 'api/UseCaseManagement.php';
        require_once 'api/UseCaseManagementDAO.php';

        require_once 'api/ConfigManagement.php';
        require_once 'api/ConfigManagementDAO.php';

        require_once 'api/LogManagement.php';
        require_once 'api/LogManagementDAO.php';

        require_once 'api/torm/torm.php';

        require_once 'api/torm/models/UseCase.php';
        require_once 'api/torm/models/Log.php';
        require_once 'api/torm/models/Scenario.php';
        require_once 'api/torm/models/Recipe.php';
        require_once 'api/torm/models/ProjectConfig.php';
        require_once 'api/torm/models/PluginConfig.php';

        require_once 'api/config.php';

    }

    function events() {

        return array(
            'EVENT_BDD_PLUGIN_ADD_USE_CASE' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_GET_USE_CASES' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_GET_ADD_TO_VCS' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_UPDATE_SCENARIO_TEST' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_GET_PLUGIN_CONFIG' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_SET_PLUGIN_CONFIG' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_SET_PROJECT_CONFIG' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_GET_PROJECT_CONFIG' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_GET_TEST_RESULTS' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_REMOVE_USE_CASES' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_EDIT_USE_CASE' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_ADD_TO_LOG' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_GET_LOGS' => EVENT_TYPE_CHAIN,
            'EVENT_BDD_PLUGIN_CLEAN_LOG' => EVENT_TYPE_CHAIN,
        );

    }

    function hooks() {

        return array(
            'EVENT_MENU_MAIN' => 'createMenu',
            'EVENT_MENU_MANAGE' => 'createMenu',
            'EVENT_BDD_PLUGIN_ADD_USE_CASE' => 'addUseCase',
            'EVENT_BDD_PLUGIN_GET_USE_CASES' => 'getUseCases',
            'EVENT_BDD_PLUGIN_GET_ADD_TO_VCS' => 'addToVCS',
            'EVENT_BDD_PLUGIN_UPDATE_SCENARIO_TEST' => 'updateScenarioTest',
            'EVENT_BDD_PLUGIN_GET_PLUGIN_CONFIG' => 'getPluginConfig',
            'EVENT_BDD_PLUGIN_SET_PLUGIN_CONFIG' => 'setPluginConfig',
            'EVENT_BDD_PLUGIN_SET_PROJECT_CONFIG' => 'setProjectConfig',
            'EVENT_BDD_PLUGIN_GET_PROJECT_CONFIG' => 'getProjectConfig',
            'EVENT_BDD_PLUGIN_GET_TEST_RESULTS' => 'getTestResults',
            'EVENT_BDD_PLUGIN_REMOVE_USE_CASES' => 'removeUseCase',
            'EVENT_BDD_PLUGIN_EDIT_USE_CASE' => 'editUseCase',
            'EVENT_BDD_PLUGIN_ADD_TO_LOG' => 'addToLog',
            'EVENT_BDD_PLUGIN_GET_LOGS' => 'getLogs',
            'EVENT_BDD_PLUGIN_CLEAN_LOG' => 'cleanLog',
        );

    }

    function getLogs($event, $params) {
        return LogManagement::getLogs();
    }

    function cleanLog($event, $params) {
        LogManagement::cleanLog();
    }

    function setPluginConfig($event, $params) {

        $gitExePath = $params["gitExePath"];
        $svnExePath = $params["svnExePath"];

        ConfigManagement::setPluginConfig($gitExePath, $svnExePath);

    }

    function addToLog($event, $params) {

        $content = $params["content"];
        $userName = $params["userName"];

        LogManagement::addToLog($content, $userName);

    }

    function createMenu() {

        $page = plugin_page("index");
        return '<a href="' . string_html_specialchars( $page ) . '">BDD</a>';

    }

    function setProjectConfig($event, $params) {

        $vcsType = $params["vcsType"];
        $vcsUrl = $params["vcsUrl"];
        $vcsUser = $params["vcsUser"];
        $vcsPassword = $params["vcsPassword"];
        $vcsLocation = $params["vcsLocation"];
        $projectNamespace = $params["projectNamespace"];
        $projectFeaturesFolder = $params["projectFeaturesFolder"];
        $projectStepsFolder = $params["projectStepsFolder"];
        $projectId = $params["projectId"];

        ConfigManagement::setProjectConfig($vcsType, $vcsUrl, $vcsUser, $vcsPassword, $projectNamespace, $projectFeaturesFolder, $projectStepsFolder, $projectId, $vcsLocation);

    }

    function updateScenarioTest($event, $params) {

        $idScenario = $params["idScenario"];
        $status = $params["status"];
        $errorMessage = base64_decode($params["errorMessageBase64"]);

        UseCaseManagement::updateScenarioTest($idScenario, $status, $errorMessage);

    }

    function addUseCase($event, $params) {

        $useCase = $params["useCase"];
        $projectId = $params["projectId"];

        return UseCaseManagement::addUseCase($useCase, $projectId);

    }

    function editUseCase($event, $params) {

        $useCase = $params["useCase"];
        $projectId = $params["projectId"];

        UseCaseManagement::removeUseCase($useCase->id);
        UseCaseManagement::addUseCase($useCase, $projectId);

    }

    function getUseCases($event, $projectId) {
        return UseCaseManagement::getUseCases($projectId);
    }

    function removeUseCase($event, $useCaseId) {
        return UseCaseManagement::removeUseCase($useCaseId);
    }

    function getTestResults($event, $projectId) {
        return UseCaseManagement::getTestResults($projectId);
    }

    function getPluginConfig($event, $param) {
        return ConfigManagement::getPluginConfig();
    }

    function getProjectConfig($event, $projectId) {
        return ConfigManagement::getProjectConfig($projectId);
    }

    function addToVCS($event, $params) {

        $useCaseId = $params["useCaseId"];
        $gherkin = base64_decode($params["gherkinBase64"]);
        $stepsDefinition = base64_decode($params["stepsDefinitionBase64"]);

        UseCaseManagement::addToVCS($useCaseId, $gherkin, $stepsDefinition);

    }

}